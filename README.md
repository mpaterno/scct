The Super-Cool CosmoSIS Tool
============================

You can install `scct` almost anywhere you like. Please do not install
it in a subdirectory of an existing CosmoSIS installation; that will
probably lead to confusion.

To establish the environment from which the `scct` commands work, run
the following in your shell (do not neglect the quotes):

~~~
$ eval "$(./bin/scct init -)"
~~~

After that, the command `scct help` will print help on how to use `scct`.

Note that some commands produce output from running CosmoSIS inside
the container. Some of this output reports relative paths. The current
working directory in the container is the `cosmosis` subdirectory
under your `scct` directory. We may want to modify the messages to
simplify things for the user.

The typical order in which to run the commands, for the first time, is
important for the first three commands.

#. `scct pull` : this obtains the base CosmoSIS image from Docker Hub.

    This contains the compilers, the libraries, the Python modules,
    etc., upon which CosmoSIS depends. You can run `scct pull`
    whenever a new CosmoSIS base image is announced; doing so will not
    affect the CosmoSIS code you have, nor and code you have written
    or CosmoSIS output you have created.
    
#. `scct clone` : this clones the CosmoSIS repositories.

    Note that the cloned repositories will be in the under the
    directory `cosmosis`. You can edit these files with whatever
    editor you like, and use whatever git client you like to maintain
    the repositories.

#. `scct image` : this creates your own, customized CosmoSIS image.

    You can repeat the `scct image` command whenever you want to
    install new system software into your customized image. To do
    this, you'll need to modify the file
    `share/docker/Dockerfile`. See the comments in that file for
    directions.
    
The remainingn commands are typically run repeatedly as you develop
and use CosmoSIS modules. Since you use your own system's editor, and
your own system's `git`, while developing, there is no `scct` command
to handle either editing or source code control.

#. `scct build` : this builds the CosmoSIS code.

    Run this command whenever you modify C, C++, or Fortran code. If
    you are modifying Python module code, running `scct build` is not
    needed.

#. `scct run` : this runs the CosmoSIS framework.

    The output files created by the run of CosmoSIS will be found
    under your `cosmosis` directory.

#. `scct postprocess` : this runs the CosmoSIS postprocessing script,
   to create statistical summaries and plots.

    The output files created by the postprocessing will be found under
    your `cosmosis` directory.
