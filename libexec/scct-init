#!/usr/bin/env bash
# Usage: scct init
# Help: Print information for how to set up the command for use.

set -e

print=""
if [ "$1" = "-" ]; then
  print=1
  shift
fi

shell="$1"
if [ -z "$shell" ]; then
  shell="$(basename "$SHELL")"
fi

resolve_link() {
  $(type -p greadlink readlink | head -1) $1
}

abs_dirname() {
  local cwd="$(pwd)"
  local path="$1"

  while [ -n "$path" ]; do
    cd "${path%/*}"
    local name="${path##*/}"
    path="$(resolve_link "$name" || true)"
  done

  pwd
  cd "$cwd"
}

root="$(abs_dirname "$0")/.."

if [ -z "$print" ]; then
  case "$shell" in
  bash )
    profile='~/.bash_profile'
    ;;
  zsh )
    profile='~/.zshenv'
    ;;
  * )
    profile='your profile'
    ;;
  esac

  { echo "# Load scct automatically by adding"
    echo "# the following to ${profile}:"
    echo
    echo "eval \"\$(${_SCCT_ROOT}/bin/scct init -)\""
    echo
  } >&2

  exit 1
fi

echo "export PATH=\"\${PATH}:${_SCCT_ROOT}/bin\""

case "$shell" in
bash | zsh )
  echo "source \"$root/completions/scct.${shell}\""
  ;;
esac

commands=(`scct commands --sh`)
IFS="|"
cat <<EOS
_scct_wrapper() {
  local command="\$1"
  if [ "\$#" -gt 0 ]; then
    shift
  fi

  case "\$command" in
  ${commands[*]})
    eval \`scct "sh-\$command" "\$@"\`;;
  *)
    command scct "\$command" "\$@";;
  esac
}
EOS

# zsh can't pass argument with aliases, but bash can.
# zsh can have functions with the name being only numbers, but bash can't.
# fix both cases here by letting zsh have a function, and bash have its alias.
case "$shell" in
bash )
  echo "alias scct=_scct_wrapper"
  ;;
zsh )
  echo "scct=_scct_wrapper"
  ;;
esac
