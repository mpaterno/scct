#!/usr/bin/env bash
# Usage: This scriptlet is not used directly.
# Help: This scriptlet is sourced by commands, to establish the appropriate
# shell variable values.
#
TOPDIR=${PWD}
BOOT_TMP=${TOPDIR}/boot_tmp
INSTALL_DIR=${TOPDIR}/cosmosis