if [[ ! -o interactive ]]; then
    return
fi

compctl -K _scct scct

_scct() {
  local word words completions
  read -cA words
  word="${words[2]}"

  if [ "${#words}" -eq 2 ]; then
    completions="$(scct commands)"
  else
    completions="$(scct completions "${word}")"
  fi

  reply=("${(ps:\n:)completions}")
}
